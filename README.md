# dwmblocks
Modular status bar for dwm written in c.

------------------------------------------------------------

# usage
To use dwmblocks first run 'make' and then install it with 'sudo make install'.
After that you can put dwmblocks in your xinitrc or other startup script to have it start with dwm.

-------------------------------------------------------------

# modifying blocks
The statusbar is made from text output from commandline programs.
Blocks are added and removed by editing the blocks.h header file.
By default the blocks.h header file is created the first time you run make which copies the default config from blocks.def.h.
This is so you can edit your status bar commands and they will not get overwritten in a future update

--------------------------------------------------------------

# requirments
"font awesome"; you can satisfy this by
installing font-awesome

--------------------------------------------------------------


sudo xbps-install -Suv font-awesome


yay -S font-awesome



sudo apt install fonts-font-awesome



sudo dnf install font-awesome

-------------------------------------------------------------

# install instructions

cd ~ 

-------------------------------------------------------------

git clone https://github.com/ACupOfLinux/dwmblocks && cd dwmblocks

# config
you have to edit the scripts in the 'scripts' folder for them to load with your home directory.


-------------------------------------------------------------
