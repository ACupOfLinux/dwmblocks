//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/ /*Command*/	 	                            /*Update Interval*/	/*Update Signal*/
    {"  ", "/home/kayden/suckless/dwmblocks/scripts/kernel",	 360,		          2},

	{"  ", "/home/kayden/suckless/dwmblocks/scripts/uptime",		 60,		          2},

	{"  ", "/home/kayden/suckless/dwmblocks/scripts/updates",  360,		          9},
	
	{"  ", "/home/kayden/suckless/dwmblocks/scripts/memory",	 6,		              1},

	{"  ", "/home/kayden/suckless/dwmblocks/scripts/vol",     0,		              10},

	{"  ", "/home/kayden/suckless/dwmblocks/scripts/clock",	     60,	              0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
